import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)

// Agregamos la URL base de nuestra API
// axios.defaults.baseURL = 'http://localhost:3000/api';
//axios.defaults.baseURL = 'https://app-mevn-bluuweb.herokuapp.com/api';
// https://app-mevn-bluuweb.herokuapp.com/

// Agregamos la URL base de nuestra API
axios.defaults.baseURL = 'http://localhost:1000/api';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

//app.get('/verify',function(req,res){
  //console.log("esto es una prueba")
//	router.push('./views/Login.vue');
//});