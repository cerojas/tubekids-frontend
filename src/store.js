import Vue from 'vue'
import Vuex from 'vuex'

import router from './router'

// para decodificar el jwt
import decode from 'jwt-decode'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    usuarioDB: '',
    acces: '',
    codigo: ''
   
   
  },

  
  mutations: {
    obtenerUsuario(state, payload){
      state.token = payload;
      if(payload === ''){
        state.usuarioDB = ''
      }else{
        state.usuarioDB = decode(payload);
        router.push({name: 'logintwo'})
      //  router.push({name: 'notas'})
        //router.push({name: 'videos'})
        //router.push({name: 'userskids'})
        }
      
    },

    obtenerUsuarioTwo(state, payload){
      state.codigo = payload;
      if(payload === ''){
        state.usuarioDB = ''
      }else{
        state.usuarioDB = decode(payload);
        //router.push({name: 'logintwo'})
        router.push({name: 'notas'})
        router.push({name: 'videos'})
        router.push({name: 'userskids'})
        }
      
    },

    

    obtenerUsuarioKid(state, payload){
      state.acces = payload;
      if(payload === ''){
        state.usuarioDB = ''
      }else{
        state.usuarioDB = decode(payload);
        router.push({name: 'playlists'})
      }
    }
  },

  


  actions: {

    guardarUsuario({commit}, payload){
      localStorage.setItem('token', payload);
      commit('obtenerUsuario', payload)
    },

    guardarUsuarioTwo({commit}, payload){
      localStorage.setItem('codigo', payload);
      commit('obtenerUsuarioTwo', payload)
    },

    guardarCodigo({commit}, payload){
      sessionStorage.setItem('codigo', payload);
      //commit('obtenerUsuario', payload)
    },

    guardarCodigoTwo({commit}, payload){
      localStorage.setItem('codigo', payload);
      //commit('obtenerUsuario', payload)
    },


    guardarUsuarioKid({commit}, payload){
      localStorage.setItem('acces', payload);
      commit('obtenerUsuarioKid', payload)
    },
    cerrarSesion({commit}){
      commit('obtenerUsuario', '');
      localStorage.removeItem('token');
      router.push({name: 'login'});
    },

    cerrarSesionkid({commit}){
      commit('obtenerUsuarioKid', '');
      localStorage.removeItem('acces');
      router.push({name: 'loginkid'});
    },

    borrarCodigo({commit}){
      commit('obtenerUsuarioTwo', '');
      localStorage.removeItem('codigo');
     // router.push({name: 'loginkid'});
    },
  //  ingresarkid(){
    //  router.push({name: 'playlists'});
   // },
    leerToken({commit}){

      const token = localStorage.getItem('token');
      if(token){
        commit('obtenerUsuario', token);
      }else{
        commit('obtenerUsuario', '');
      }

    },

    leerTokenKid({commit}){

      const acces = localStorage.getItem('acces');
      if(acces){
        commit('obtenerUsuarioKid', acces);
      }else{
        commit('obtenerUsuarioKid', '');
      }

    },

    leerTokenTwo({commit}){

      const codigo = localStorage.getItem('codigo');
      if(codigo){
        commit('obtenerUsuarioTwo', codigo);
      }else{
        commit('obtenerUsuarioTwo', '');
      }

    }

  

  },
  getters:{

    estaActivoTwo: state => !!state.codigo,
    
    estaActivokid: state => !!state.acces,
    
    estaActivo: state => !!state.token

    

    }

     

    
  
})
