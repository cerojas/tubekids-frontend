import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

import store from './store'

Vue.use(Router)

const router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/notas',
      name: 'notas',
      component: () => import(/* webpackChunkName: "about" */ './views/Notas.vue'),
      meta: {requireAuth: true}
    },
    {
      path: '/videos',
      name: 'videos',
      component: () => import(/* webpackChunkName: "about" */ './views/Video.vue'),
      meta: {requireAuth: true}
    },
    {
      path: '/userskids',
      name: 'userskids',
      component: () => import(/* webpackChunkName: "about" */ './views/UserKid.vue'),
      meta: {requireAuth: true}
    },

    
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue')
    },
    {
      path: '/loginkid',
      name: 'loginkid',
      component: () => import(/* webpackChunkName: "about" */ './views/Loginkid.vue')
    },
    {
      path: '/playlists',
      name: 'playlists',
      component: () => import(/* webpackChunkName: "about" */ './views/Playlist.vue')
    },
    {
      path: '/registers',
      name: 'registers',
      component: () => import(/* webpackChunkName: "about" */ './views/Register.vue')
    },
    {
      path: '/logintwo',
      name: 'logintwo',
      component: () => import(/* webpackChunkName: "about" */ './views/Logintwo.vue')
    }



  ]
})

router.beforeEach((to, from, next) => {

  const rutaProtegida = to.matched.some(record => record.meta.requireAuth)

  if(rutaProtegida && store.state.token === ''){

    next({name: 'login'});

  }else{
    next();
  }


});


export default router;
